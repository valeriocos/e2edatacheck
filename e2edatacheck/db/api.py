# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#

import elasticsearch_dsl as dsl
import logging


logger = logging.getLogger(__name__)


def count_docs(conn, myindex, origin):
    """Return number of documents found in the index matching the origin

    :param conn: ES connection
    :param myindex: name of the index
    :param origin: name of the origin
    :return: integer with the hits matched
    """
    s = dsl.Search(using=conn, index=myindex) \
            .filter('term', origin=origin)
    response = s.execute()
    return response.hits.total

def count_docs_per_bucket(conn, myindex, origin, field):
    """Returns number of documents for a given origin and creating buckets by field

    :param conn: ES connection
    :param myindex: index name
    :param origin: string to be search as origin field in the search
    :param field: field used to create buckets
    :return: dictionary with the result of the query
    """

    buckets = 10000

    s = dsl.Search(using=conn, index=myindex) \
            .filter('term', origin=origin)
    s.aggs.bucket(field, 'terms', field=field, size=buckets) \
          .metric('n_docs', 'value_count', field=field)
    response = s.execute()

    return response.aggregations[field]
